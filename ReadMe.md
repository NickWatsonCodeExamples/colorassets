# Color Assets
Following the migration of the re-skined Beta Quiz app into the simplified Instore structure I found that creating and modifying Colors in the app was a total pain
Colors set in JSON, Colors set in ColorDefaults and Colors set in UIColor+Defaults. Had to be an easier way and if we were targeting iOS11 we could use the new UIColor(named: "assetColor") and have an Asset Color method but we are not and are targeting iOS9.
I came up with a solution whereby we can now use a structure of having an ColorAssets in the app for each target. This way you can set specific colors in the Colors.xcassets once
This adds iOS 9+ compatibility for named colors in asset catalogs.
It extends UIColor with a new optional initializer: UIColor(asset:) that works in the same way as UIColor(named:).

**Note:** - This will only work in code. Named colors set in Interface Builder will still not work on iOS 9 or 10!

## Installation

    1.	add the repository via carthage (in my private repo atm)
	2.	Add an Asset Catalog to your project called Colors.xcassets
	3.	Add a New Copy Files Phase to your target that copies Colors.xcassets to the Resources destination (leave Subpathblank)

## Notes

Copying Colors.xcassets to your app's Resources directory bypasses Apple's optimisations around Asset Catalogs.
You can name your .xcassets file something else: declare ColorAssetsManager.shared.catalogName = "OtherName" before using UIColor(asset:).
You can set the Bundle where your asset catalog resides: set ColorAssetsManager.shared.bundle before using UIColor(asset:) or use UIColor(asset:in:compatibleWith:).
On iOS 11+, UIColor(asset:) just calls the native UIColor(named:).
By default, colors are lazily loaded to avoid re-parsing the asset JSON on subsequent use. Cached colors are released if the app receives a memory warning.
You can disable in-memory caching of colors using ColorAssetsManager.shared.cachingEnabled = false.
Device-specific colors are supported except Watch and Mac. You can toggle them for the Color Set in the Inspector.
P3 (wide gamut) specific colors are supported on iOS 10+. You can toggle them for the Color Set in the Inspector.
During testing its best to set the colors as floating point and NOT 8 bit

## Usage

I have created one global ColorPalette Struct and colors are now set in that class that point to the Colors Assets Catalogue
usage in ColorPalette
static let primaryColor = UIColor(asset: "primaryColor")
In code we can now just call ColorPalette.primaryColor


